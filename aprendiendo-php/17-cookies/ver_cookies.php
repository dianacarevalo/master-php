<?php

/*
  para mostrar el valor de las cookies tengo que usar $_COOKIE, es una
 * variable superglobal, es un array asociativo
 *  */

if (isset($_COOKIE['micookie'])) {
    echo "<h1>" . $_COOKIE['micookie'] . "</h1>";
} else {
    echo 'No existe la cookie';
}


if (isset($_COOKIE['unyear'])) {
    echo "<h1>" . $_COOKIE['unyear'] . "</h1>";
} else {
    echo 'No existe la cookie unyear';
}

?>

<a href="crear_cookies.php">Crear cookies</a>
<a href="borrar_cookies.php">Borrar mis cookies</a>