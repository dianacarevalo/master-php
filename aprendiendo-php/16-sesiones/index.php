<?php
/* Sesión: almacenar y persisitir datos del usuario mientras que navega
 * en un sitio web, hasta que cierra sesión o cierra el navegador.
 * 
 */

//iniciar la sesión

session_start();

//Variable local
$variable_normal = "Soy una cadena de texto";

//variable de sesión
$_SESSION['variable_persistente'] = "HOLA SOY UNA SESIÓN ACTIVA";

echo $variable_normal;