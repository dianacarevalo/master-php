<!DOCTYPE HTML>
<html lang="es">
    <head>
        <meta charset="uft-8"/>
        <title>Includes en PHP</title>
    </head>
    <body>
        <?php 
        $nombre = "Carolina";
        ?>
        <!--cabecera-->
        <div class="cabecera">
            <h1>Include en PHP</h1>
            <ul>
                <li><a href="index.php">Inicio</a></li>
                <li><a href="sobremi.php">Sobre mi</a></li>
                <li><a href="contacto.php">Contacto</a></li>
                <li><a href="https://google.es" target="_blank">Google</a></li>
            </ul>
            <hr/>
        </div>

