<?php

/*
  recoger dos números por url(parámetro get) y hacer las operaciones básicas de esos números
 */

//SOLUCIÓN DEL CURSO

if (isset($_GET['numero1']) && isset($_GET['numero2'])) {
    $numero1 = $_GET['numero1'];
    $numero2 = $_GET['numero2'];

    echo '<h1>Calculadora</h1>';
    echo 'Suma: ' . ($numero1 + $numero2) . '<br/>';
    echo 'Resta: ' . ($numero1 - $numero2) . '<br/>';
    echo 'Multiplicación:  ' . ($numero1 * $numero2) . '<br/>';
    echo 'División: ' . ($numero1 / $numero2) . '<br/>';
    echo 'Módulo o resto: ' . ($numero1 % $numero2);

    
} else {
    echo "<h3>Introduce correctamente los valores por la URL";
}

//MI SOLUCIÓN

//if (isset($_GET['numero1'])) {
//    // Casteo: cambiar tipo de dato
//    $numero1 = (int) $_GET['numero1'];
//} else {
//    $numero1 = 1;
//}
//if (isset($_GET['numero2'])) {
//    // Casteo: cambiar tipo de dato
//    $numero2 = (int) $_GET['numero2'];
//} else {
//    $numero2 = 1;
//}

//echo 'Suma: ' . ($numero1 + $numero2) . '<br/>';
//echo 'Resta: ' . ($numero1 - $numero2) . '<br/>';
//echo 'Multiplicación:  ' . ($numero1 * $numero2) . '<br/>';
//echo 'División: ' . ($numero1 / $numero2) . '<br/>';
//echo 'Módulo o resto: ' . ($numero1 % $numero2);

