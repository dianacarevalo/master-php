<?php

//crear variables pais y continente e inicializarlas para imprimir por pantalla
// poner en un comentario qué tipo de dato tienen 

$pais = "Colombia"; // string
$continente = "América"; // string

echo "<h3>El pais es: $pais</h3>";
echo "<h3>El continente es: $continente</h3>";

echo "El tipo de dato de $pais es: ".gettype($pais)."<br/>";
echo "El tipo de dato de $continente es: ".gettype($continente);
