<?php

/*
  mostrar todos los números impares entre dos números que nos lleguen por la URL
 * 
 */

if (isset($_GET['numero1']) && isset($_GET['numero2'])) {
    $numero1 = $_GET['numero1'];
    $numero2 = $_GET['numero2'];


    if ($numero1 < $numero2) {
        echo "<h3>Los números impares contenidos entre $numero1 y $numero2 son: </h3>";
        for ($i = $numero1 + 1; $i < $numero2; $i++) {
            if ($i % 2 != 0) {
                echo "$i <br/>";
            }
        }
    } else {
        echo "<h3>El número 1 debe ser menor al número 2</h3>";
    }
} else {
    echo "<h3>Introduce correctamente los valores por la URL";
}