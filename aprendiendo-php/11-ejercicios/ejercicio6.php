<?php

/*
  imprimir por pantalla todas las tablas de multiplicar del 1 al 10
 * dentro de una tabla html
 */

echo"<table border='1'><tr>";
echo "<tr>";
for ($cabecera = 1; $cabecera <= 10; $cabecera++) {
    echo "<td>Tabla del $cabecera</td>";
}
echo '</tr>';
echo '<tr>';
for ($j = 1; $j <= 10; $j++) {
    echo '<td>';
    for ($i = 1; $i <= 10; $i++) {
        echo "$j x $i = " . ($j * $i) . "<br/>";
    }
}
echo '</tr>';

