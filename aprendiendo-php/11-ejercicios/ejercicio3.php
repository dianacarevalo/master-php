<?php

/*
  imprimir por pantalla los cuadrados de los 40 primeros números naturales
  usar while
 *
 */

$numero = 0;

while ($numero <= 40) {
    echo "El cuadrado de $numero es: " . ($numero * $numero) . '<br/>';
    $numero++;
}

//for ($i = 0; $i <=40; $i++) {
//    echo "El cuadrado de $i es: " . ($i * $i) . '<br/>'; 
//}