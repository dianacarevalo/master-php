<?php

/*
  ARRAY: colección o conjunto de datos bajo un único nombre
 */

$peliculas = array('Batman', 'Spiderman', 'Split');

//var_dump($peliculas[2]);

$cantantes = ['2pac', 'Michael', 'Redimi2'];

//var_dump($cantantes);

$personas = array(
    'nombre' => 'Carolina',
    'apellidos' => 'Arévalo',
    'web' => 'carolina.com'
);

echo $personas['nombre'] . '<br/>';

echo $peliculas[0];
echo '<br/>';
echo $cantantes[2];

//Recorrer con for

echo '<ul>';
echo "<h3>Listado de películas";
for ($i = 0; $i < count($peliculas); $i++) {
    echo "<li>" . $peliculas[$i] . "</li>";
}
echo '</ul>';

//Recorrer con foreach

echo '<ul>';
echo "<h3>Listado de cantantes";
foreach ($cantantes as $cantante) {
    /* foreach ($cantantes as $key => $value) {  
      echo "<li>".$cantantes[$key]."</li>"; */
    echo "<li>" . $cantante . "</li>";
}
echo '</ul>';

echo '<br/>';
//Arrays multidimensionales

$contactos = array(
    array(
        'nombre' => 'Carolina',
        'email' => 'carolina@gmail.com'
    ),
    array(
        'nombre' => 'Camila',
        'email' => 'camila@gmail.com'
    ),
    array(
        'nombre' => 'Daniela',
        'email' => 'Daniela@gmail.com'
    ),
);

var_dump($contactos);

echo $contactos[1]['nombre'];
echo '<br/>';
echo $contactos[2]['email'];

foreach ($contactos as $key => $contacto) {
    var_dump($contacto['nombre']);
}