<?php

$cantantes = ['2pac', 'Michael', 'Redimi2', 'Alex'];
$numeros = [1, 2, 5, 8, 3, 4];

//ofdenar
asort($cantantes);
var_dump($cantantes);

sort($numeros);
var_dump($numeros);

//añadir elementos a un array

$cantantes[] = "Natos";
array_push($cantantes, 'Funky');

//eliminar elementos del array
array_pop($cantantes);
unset($cantantes[4]);
var_dump($cantantes);

//Sacar un elemento Aleatorio del array
//echo array_rand($cantantes);
$indice = array_rand($cantantes);
echo $cantantes[$indice];

//Dar la vuelta o reverse

var_dump(array_reverse($numeros));

//Buscar en un array
$resultado = array_search('Michael', $cantantes);
var_dump($resultado);

//contar los elementos de un array

echo count($cantantes);
