<?php

//abrir archivo
$archivo = fopen("fichero_texto.txt", "a+");

//leer contenido
while (!feof($archivo)) {
    $contenido = fgets($archivo);
    echo $contenido."<br/>";
}

//Escribir en el archivo
fwrite($archivo, "***Soy un texto metido desde php***");

//cerrar archivo
fclose($archivo);

//copiar
//copy('fichero_texto.txt','fichero_copiado.txt') or die ("Error al copiar");

//renombrar
//rename('fichero_copiado.txt', 'archivito_copiadito.txt');

//eliminar
//unlink('archivito_copiadito.txt') or die ('Error al borrar');

//validar que existe
if (file_exists('fichero_texto.txt')) {
    echo 'El archivo existe';
}  else {
    echo 'El archivo NO existe';    
}