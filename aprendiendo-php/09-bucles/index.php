<?php

//BUCLE WHILE
// Estructura que itera o repite la ejecución de una serie de instrucciones tantas veces como sea necesario con base en una condición dada


$numero = 0;
while ($numero <= 10) {
    //echo '<p>' . $numero . '</p>';
    //echo "$numero," ;
    echo $numero;
    if ($numero != 10) {
        echo ',';
    }
    $numero++;
}

echo '<hr>';

//EJEMPLO

if (isset($_GET['numero'])) {
    // Casteo: cambiar tipo de dato
    $numero = (int) $_GET['numero'];
} else {
    $numero = 1;
}

//var_dump($numero);

echo "<h1>Tabla de multiplicar del numero $numero</h1>";

$contador = 0;
while ($contador <= 10) {
    echo "$numero x $contador = " . ($numero * $contador) . "<br/>";
    $contador++;
}

echo '<hr/>';
// DO WHILE

$edad = 18;
$contador = 1;

do{
    echo "Tienes acceso al local privado $contador<br/>";
    $contador++;
}while($edad >= 18 && $contador <=10);
