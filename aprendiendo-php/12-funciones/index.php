<?php

/*
  function nombreDeMiFuncion($mi_parametro){
  bloque de instrucciones
  }
 * 
  para llamarla:
  nombreDeMiFuncion($mi_parametro):
 */

//ejemplo 1
function muestraNombres() {
    echo "Victor Robles <br/>";
    echo "Camila Rodríguez <br/>";
    echo "Daniela Arévalo <br/>";
    echo "Carolina Arévalo <br/>";
}

//invocar función
muestraNombres();

//ejemplo 2
echo '<hr>';

function tabla($numero) {
    echo "<h3>Tabla de multiplicar del $numero<br/>";
    for ($i = 1; $i <= 10; $i++) {
        echo "$numero x $i =" . ($numero * $i) . "<br/>";
    }
}

if (isset($_GET['numero'])) {
    tabla($_GET['numero']);
} else {
    echo "Debe ingresar el número por la URL";
}

//for ($i = 1; $i <10; $i++) {
//    tabla($i);
//}

echo "<hr/>";

//ejemplo 3
function calculadora($numero1, $numero2) {
    $suma = $numero1 + $numero2;
    $resta = $numero1 - $numero2;
    $multi = $numero1 * $numero2;
    $division = $numero1 / $numero2;

    echo "Suma: $suma </br>";
    echo "Resta: $resta </br>";
    echo "Multiplicación: $multi </br>";
    echo "División: $division </br>";
}

calculadora(10, 30);
echo "<hr/>";

//ejemplo con retorno

function devuelveNombre($nombre) {
    return "El nombre es: $nombre";
}

echo devuelveNombre("Carolina");

echo "<hr/>";

//mejora del ejemplo 3 con retorno y parámetro opcional
function calculadora2($numero1, $numero2, $negrita = false) {
    $suma = $numero1 + $numero2;
    $resta = $numero1 - $numero2;
    $multi = $numero1 * $numero2;
    $division = $numero1 / $numero2;

    $cadena_texto = "";

    if ($negrita) {
        $cadena_texto.= "<h2>";
    }

    $cadena_texto .= "Suma: $suma </br>";
    $cadena_texto .= "Resta: $resta </br>";
    $cadena_texto .= "Multiplicación: $multi </br>";
    $cadena_texto .= "División: $division </br>";

    if ($negrita) {
        $cadena_texto .= "</h1>";
    }

    return $cadena_texto;
}

echo calculadora2(10, 30, true);

echo "<hr/>";

function getNombre($nombre) {
    $texto = "El nombre es $nombre";
    return $texto;
}

function getApellidos($apellidos) {
    $texto = "Los apellidos son $apellidos";
    return $texto;
}

function devuelveNombreApellido($nombre, $apellidos) {
    $texto = getNombre($nombre)
            ."<br/>".
            getApellidos($apellidos);
    
    return $texto;
}

echo devuelveNombreApellido("Carolina", "Arévalo");
