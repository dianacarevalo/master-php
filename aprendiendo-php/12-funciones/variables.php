<?php

/*
  Variables globales y locales
 */

$frase = "Ni los genios son tan genios, ni los mediocres tan mediocres";

echo $frase;

function holaMundo() {
    global $frase;
    echo "<h1>$frase</h1>";

    $year = 2019;
    echo "<h1>$year</h1>";

    return $year;
}

echo holaMundo();

//echo $year;  no lo puedo hacer porque la variable esta dentro de la función

//FUNCIONES VARIABLES

echo "<hr/>";

function buenasDias() {
    return "HOla, Buenos días :)";
}

function buenasTardes() {
    return "Hey! Qué tal ha ido la comida?";
}

function buenasNoches() {
    return "¿Te vas a dormir ya?";
}

//opción 1
$horario = "buenasNoches";
echo $horario();

echo "<hr/>";
//opción 2
$horario = "Noches";
$mi_funcion = "buenas" . $horario;
echo $mi_funcion();

echo "<hr/>";
//opción 3
$horario = $_GET['horario'];
$mi_funcion = "buenas" . $horario;
echo $mi_funcion();

