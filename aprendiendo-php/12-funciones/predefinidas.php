<?php

$nombre = "Carolina";
var_dump($nombre);

echo date('d-m-Y');
echo "<br>" . time();
echo "<br/>";
echo "Raiz cuadrada de 10: " . sqrt(10);
echo "<br/>";
echo "aleatorio" . rand(10, 40);
echo "<br/>";
echo 'Número pi ' . pi();
echo "<br/>";
echo 'Redondear ' . round(7.596, 2);

//MAS FUNCIONES

echo "<br/>";
echo gettype($nombre);
echo "<br/>";
if (is_string($nombre)) {
    echo 'Es un string';
}

echo "<br/>";
if (isset($edad)) {
    echo 'La variable existe';
} else {
    echo 'La variable no existe';
}

//limpiar espacios
echo "<br/>";
$frase = "                  mi contenido         ";
echo trim($frase);

echo "<br/>";
//eliminar variables o indices de arrays
$year = 2020;
unset($year);

echo '<br/>';
$texto = "";
if (empty($texto)) {
    echo "La variable texto está vacía";
} else {
    echo "La variable texto tiene contenido";
}

echo '<br/>';
//contar caracteres de un string
$cadena = "12345";
echo strlen($cadena);

echo '<br/>';
//encontrar en un string la posición determinada
$frase = "La vida es bella";
echo strpos($frase, "vid");

echo '<br/>';
//reemplazar contenido de un string
$frase = str_replace("vida", "moto", $frase);
echo $frase;

echo '<br/>';
//permite convertir a mayúscula y minúscula
echo strtolower($frase).'<br/>';
echo strtoupper($frase);