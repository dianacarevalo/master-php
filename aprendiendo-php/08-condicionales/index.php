<?php

/*
  //CONDICIONALES
  IF:
  if(condidion){
  instrucciones
  }else{
  otras instrucciones
  }

  //OPERADORES DE COMPARACIÓN
  == igual
  === identidico, o sea que sea el mismo tipo de dato
  != diferente
  <> diferente
  !== no identico
  < menor que
  > mayor que
  <= menor o igual
  >= mayor o igual
 * 
  //OPERADORES LÓGICOS
  && AND
  || OR
  !  NOT
  and, or
 */

//ejemplo 1
$color = "rojo";

if ($color == "rojo") {
    echo 'El color es rojo';
} else {
    echo 'El color NO es rojo';
}

echo '<hr>';

//ejemplo 2
$year = 2019;

if ($year < 2019) {
    echo 'Es un año anterior al 2019';
} else {
    echo 'Es un año posterior o igual al 2019';
}

//ejemplo 3
$nombre = "David Rojas";
$edad = 49;
$mayoria_edad = 18;
$ciudad = "Madrid";
$continente = "Europa";

if ($edad >= $mayoria_edad) {
    echo "<h1>$nombre es mayor de edad</h1>";
    if ($continente == "Europa") {
        echo "<h2> y es de $ciudad";
    } else {
        echo 'No es Europeo';
    }
} else {
    echo "<h2> $nombre no es mayor de edad </h2>";
}

echo '<hr>';
//ejemplo 4

$dia = 3;

if ($dia == 1) {
    echo 'Es lunes';
} elseif ($dia == 2) {
    echo 'Es martes';
} elseif ($dia == 3) {
    echo 'Es miércoles';
} elseif ($dia == 4) {
    echo 'Es jueves';
} elseif ($dia == 5) {
    echo 'Es viernes';
} else {
    echo 'Es fin de semana';
}

echo '<br>';
//SWITCH
$dia = 4;

switch ($dia) {
    case 1:
        echo 'Es lunes';
        break;
    case 2:
        echo 'Es martes';
        break;
    case 3:
        echo 'Es miércoles';
        break;
    case 4:
        echo 'Es jueves';
        break;
    case 5:
        echo 'Es viernes';
        break;
    default :
        echo 'Es fin de semana';
}


echo '<hr>';
//ejemplo 5

$edad1 = 18;
$edad2 = 64;
$edad_oficial = 20;

if ($edad_oficial >= $edad1 && $edad_oficial <= $edad2) {
    echo 'Esta en edad de trabajar';
} else {
    echo 'No puede trabajar';
}

echo '<hr>';

$pais = "España";

if ($pais == "Mexico" || $pais == "España" || $pais == "Colombia") {
    echo 'En este pais se habla español';
} else {
    echo 'No se habla español';
}

echo '<hr>';
//GOTO
goto marca;
echo "<h3>Instrucción 1 </h3>";
echo "<h3>Instrucción 2 </h3>";
echo "<h3>Instrucción 3 </h3>";
echo "<h3>Instrucción 4 </h3>";

marca:
echo 'Me he saltado 4 echos';