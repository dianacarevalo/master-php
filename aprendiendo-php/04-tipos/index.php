<?php

/* 
TIPOS DE DATOS:
entero (int / integer)
coma flotante
cadenas
boleano
null
array
objetos
 */

$numero = 100;
$decimal = 27.9;
$texto = 'Soy un texto \' ';
$verdadero = true;

echo($texto);
echo gettype($numero);
echo gettype($decimal);
echo gettype($verdadero);

//Debugear

$mi_nombre = 'Carolina Arevalo';
//var_dump($mi_nombre);

