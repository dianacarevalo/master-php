<?php
/*
  crear un array con el contenido de la siguiente tabla:
 * columna = indice
 * tabla de videojuegos de:
 * ACCIÓN   AVENTURA    DEPORTES
 * GTA      ASSASINS    FIFA 19
 * COD      CRASH       PES 19
 * PUGB     PRINCE      MOTO GP 19
 * 
 * cada fila debe ir en un fichero separado include
 */

$videojuegos = array(
    'ACCION' => array('GTA', 'COD', 'PUGB'),
    'AVENTURA' => array('ASSASINS', 'CRASH', 'PRINCE'),
    'DEPORTES' => array('FIFA 19', 'PES 19', 'MOTO GP 19')
);

$categorias = array_keys($videojuegos);
?>

<table border="1">
    <?php require_once 'includes/encabezado.php'; ?>
    <?php require_once 'includes/primera.php'; ?>
    <?php require_once 'includes/segunda.php'; ?>
    <?php require_once 'includes/tercera.php'; ?>
</table>
