<?php

/*
  compruebe si una variable está vacia, si esta vacía llenarla
 * con datos en minúscula, pero mostrarlos en mayúscula y negrita.
 */

$variable = "";

if (empty($variable)) {
    $variable = strtolower("Carolina Arevalo");
    echo '<strong>' . strtoupper($variable) . '</strong>';
} else {
    echo "La variable tiene este contenido: " . $variable;
}


