<?php

/*
  hacer un array con 8 números
 * recorrerlo y mostrarlo
 * ordenarlo y mostrarlo
 * mostrar su longitud
 * buscar algo dentro del array con un parámetro por url
 */

$numeros = [5, 10, 7, 12, 23, 1, 4, 9];

function mostrarArray($numeros) {
    $resultado = "";
    foreach ($numeros as $numero) {
        $resultado.= $numero . '<br/>';
    }
    return $resultado;
}

echo "<h3>Recorrer y mostrar</h3>";
echo mostrarArray($numeros);

echo "<h3>Ordenar y mostrar</h3>";
sort($numeros);
//var_dump($numeros);
echo mostrarArray($numeros);

echo '<br/>';

echo "<h3>El tamaño del array es: " . count($numeros) . "</h3>";

echo '<br/>';

if (isset($_GET['numero'])) {
    $busqueda = $_GET['numero'];

    echo "<h3>Vamos a buscar el número $busqueda</h3>";
    $search = array_search($busqueda, $numeros);
    if (!empty($search)) {
        echo 'El número buscado existe en el array, en el índice ' . $search;
    } else {
        echo 'El número no existe en el array';
    }
} else {
    echo 'Ingrese un número a buscar por la url';
}
