<?php

/*
  crear un script que tenga 4 variables, una tipo array, una tipo string
 * otra int y otra boleano y que imprima un mensaje según el tipo de
 * variable que sea.
 */

$arreglo = array("Hola", 88);
$texto = "Hola";
$numero = 15;
$boleano = true;

function mensajeSegunDato($variable) {
    $tipo = gettype($variable);
    if ($tipo == 'array') {
        echo "<h3>El dato es tipo arreglo</h3>";
    } elseif ($tipo == 'string') {
        echo "<h3>El dato es tipo string</h3>";
    } elseif ($tipo == 'integer') {
        echo "<h3>El dato es tipo numérico</h3>";
    } elseif ($tipo == 'boolean') {
        echo "<h3>El dato es tipo string</h3>";
    }
}

echo mensajeSegunDato($arreglo);
echo '<hr/>';

// Solución del profesor
if (is_string($texto)) {
    echo "<h3>El dato es tipo texto</h3>";
}
if (is_array($arreglo)) {
    echo "<h3>El dato es tipo arreglo</h3>";
}
if (is_bool($boleano)) {
    echo "<h3>El dato es tipo string</h3>";
}
if (is_integer($numero)) {
    echo "<h3>El dato es tipo numérico</h3>";
}