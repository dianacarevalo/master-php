<?php

if (!is_dir('mi_carpeta')) {
    //crear carpeta
    mkdir('mi_carpeta', 0777) or die('No se puede crear la carpeta');
} else {
    echo 'Ya existe la carpeta';
}

echo '<hr/>';
//eliminar carpeta
//rmdir('mi_carpeta');

if ($gestor = opendir('./mi_carpeta')) {
    while (false !== ($archivo = readdir($gestor))) {
        if ($archivo != '.' && $archivo != '..') {
            echo $archivo . '<br/>';
        }
    }
}
